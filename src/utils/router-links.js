const Util = (name) => {
  const array = {
    Login: '/auth/login',
    Dashboard: '/',
    'Quản lý khóa học': '/user',
    'TÀI KHOẢN NGƯỜI DÙNG': 'userAccount',
    'TÀI KHOẢN HỆ THỐNG': 'systemAccount',
    'VAI TRÒ': 'roleAccount',
    banner: 'banner',
    notification: '/notification',
    'Quản lý khóa học': '/Course',
    'TÀI KHOẢN NGƯỜI DÙNG': '/userAccount',
    'TÀI KHOẢN HỆ THỐNG': '/systemAccount',
    'VAI TRÒ': '/roleAccount',
    createCourse: '/createCourse',
  }; // 💬 generate link to here

  // const apis = {
  //   Dashboard: '/dashboard',
  //   User: 'users',
  // }; // 💬 generate api to here

  return array[name];
};
export default Util;
