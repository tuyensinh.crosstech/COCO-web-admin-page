import { Input, Space } from 'antd';
import React from 'react';
const { Search } = Input;
const onSearch = (value) => console.log(value);
const App = () => (
  <Space direction="vertical">
    <Search
      style={{ borderRadius: 50 }}
      placeholder="input search text"
      onSearch={onSearch}
      enterButton
    />
  </Space>
);
export default App;
