import swal from 'sweetalert';

export const showDeleteUserModal = (onAccept) => {
  swal({
    title: 'Bạn có chắc không?',
    text: `Sau khi xóa , bạn sẽ không thể khôi phục lại. Bạn có muốn tiếp tục?`,
    icon: 'warning',
    // dangerMode: true,
    buttons: ['Hủy', 'Đồng Ý'],
  }).then((yes) => {
    if (yes) {
      onAccept();
    }
  });
};

export const showLockUserModal = (action, onAccept) => {
  swal({
    title: action
      ? `Bạn có muốn khóa tài khoản này không?`
      : 'Bạn có muốn mở tài khoản này không?',
    text: action
      ? `Khi khóa tài khoản tài khoản này sẽ không thể sử dụng được nữa`
      : 'Khi mở tài khoản tài khoản này sẽ có thể sử dụng lại',
    icon: 'warning',
    // dangerMode: true,
    buttons: ['Hủy', 'Đồng Ý'],
  }).then((yes) => {
    if (yes) {
      onAccept();
    }
  });
};

export const showApproveModal = (action, onAccept) => {
  swal({
    title: action
      ? `Bạn có muốn người này trở thành Mentor?`
      : 'Bạn có muốn từ chối người này trở thành mentor?',
    text: action ? `` : '',
    icon: 'warning',
    // dangerMode: true,
    buttons: ['Hủy', 'Đồng Ý'],
  }).then((yes) => {
    if (yes) {
      onAccept();
    }
  });
};

export const showConfirmSuccess = () => {
  swal({
    title: 'Thực hiện thành công',
    text: `Thành công`,
    icon: 'success',
    // dangerMode: true,
    buttons: 'Đồng ý',
  });
};
export const showConfirmError = () => {
  swal({
    title: 'Thực hiện thất bại',
    text: `Thất bại`,
    icon: 'error',
    // dangerMode: true,
    buttons: 'Đồng ý',
  });
};

export const showDeleteBanner = (name, onAccept) => {
  swal({
    title: `Bạn có muốn xóa banner này?`,
    text: `Khi xóa ${name} không thể khôi phục bạn có muốn tiếp tục?`,
    icon: 'warning',
    // dangerMode: true,
    buttons: ['Hủy', 'Đồng Ý'],
  }).then((yes) => {
    if (yes) {
      onAccept();
    }
  });
};
