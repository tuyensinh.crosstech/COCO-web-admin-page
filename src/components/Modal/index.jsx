import { Button, Card, Modal } from 'antd';
import './index.less';
const ModalConfim = ({
  open = false,
  setOpen,
  title,
  alertModal,
  content,
  button,
  handle,
  styleModal,
}) => {
  // const showModal = () => {
  //   setIsModalOpen(true);
  // };
  const handleOk = () => {
    handle();
    setOpen(false);
  };

  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <>
      {/* <Button className="btnModal" onClick={showModal}>
        {children}
      </Button> */}
      {/* <div className="modalCoco"> */}
      <Modal
        className={'modalCoco ' + styleModal}
        onOk={handleOk}
        onCancel={handleCancel}
        title={title}
        open={open}
        footer={null}
      >
        <div className="contentModal">
          {alertModal ? <div className="alertModal">{alertModal}</div> : <></>}
          {content ? <Card className="cardContent">{content}</Card> : <></>}
        </div>
        <div className={button.length < 2 ? 'clone' : 'btnModal'}>
          <Button
            className="closeModal"
            onClick={() => {
              handleCancel();
            }}
          >
            {button[0]}
          </Button>
          <Button
            className="handleModal"
            onClick={() => {
              handleOk();
            }}
          >
            {button[1]}
          </Button>
        </div>
      </Modal>
    </>
  );
};
export default ModalConfim;
