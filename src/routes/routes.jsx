import { DashboardOutlined } from '@ant-design/icons';
import React, { lazy } from 'react';
import routerLinks from '@/utils/router-links';

const routes = [
  {
    label: 'dashboard',
    path: routerLinks('Dashboard'),
    component: React.lazy(() => import('@/pages/Dashboard/Dashboard')),
    key: '/',
  },
  // {
  //   path: '*',
  //   layout: false,
  //   component: React.lazy(() => import('@/pages/NotFound/404')),
  // },
  {
    label: 'khoa hoc',
    path: routerLinks('Quản lý khóa học'),
    component: React.lazy(() => import('@/pages/Course')),
    key: 'banner',
  },
  {
    label: 'createCourse',
    path: routerLinks('createCourse'),
    component: React.lazy(() => import('@/pages/Course/createCourse')),
    key: 'createCourse',
  },
  {
    label: 'account',
    path: routerLinks('TÀI KHOẢN NGƯỜI DÙNG'),
    component: React.lazy(() => import('@/pages/Account')),
    key: 'account',
  },
  {
    label: 'role',
    path: routerLinks('VAI TRÒ'),
    component: React.lazy(() => import('@/pages/Banner')),
    key: 'role',
  },
  {
    label: '404',
    path: routerLinks('TÀI KHOẢN HỆ THỐNG'),
    component: React.lazy(() => import('@/pages/NotFound/404')),
    key: '404',
  },
  {
    path: routerLinks('notification'),
    component: React.lazy(() =>
      import('@/pages/Notification/NotificationPage')
    ),
  },
  {
    path: `${routerLinks('notification')}/:id`,
    component: React.lazy(() =>
      import('@/pages/Notification/components/NotficationForm/NotificationForm')
    ),
  },
  {
    path: `${routerLinks('notification')}/edit/:id`,
    component: React.lazy(() =>
      import('@/pages/Notification/components/NotficationForm/NotificationForm')
    ),
  },
  {
    path: `${routerLinks('notification')}/add`,
    component: React.lazy(() =>
      import('@/pages/Notification/components/NotficationForm/NotificationForm')
    ),
  },
  {
    label: 'infoUser',
    path: 'userAccount/:id',
    component: React.lazy(() => import('@/pages/Account/component/infoUser')),
    key: 'info',
  },
  {
    path: routerLinks('banner'),
    component: React.lazy(() => import('@/pages/Banner')),
  },
];

export default routes;
