const useSyncDataTable = ({ request }) => {
  const [tableData, setTableData] = useState({
    data: [],
    total: 0,
  });

  const fetchRows = useCallback(
    async (params, sortModel) => {
      if (!request) return;

      const sort = sortModel?.at(0) || {};
      const res = await request({
        ...params,
        arrangement: sort?.sort?.toUpperCase() || '',
        order: sort?.field || '',
      });
      setTableData((pre) => ({
        ...pre,
        data: res?.data || [],
        total: res?.count || 0,
      }));
    },
    [request]
  );

  return {
    tableData,
    fetchRows,
  };
};

export default useSyncDataTable;
