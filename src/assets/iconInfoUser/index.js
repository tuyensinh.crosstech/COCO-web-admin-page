import Cake from '@/assets/iconInfoUser/Icon-Cake.svg';
import EnvelopeSimple from '@/assets/iconInfoUser/Icon-EnvelopeSimple.svg';
import GenderFemale from '@/assets/iconInfoUser/Icon-GenderFemale.svg';
import MapPin from '@/assets/iconInfoUser/Icon-MapPin.svg';
import PhoneCall from '@/assets/iconInfoUser/Icon-PhoneCall.svg';
import UserCircle from '@/assets/iconInfoUser/UserCircle.svg';
import Introduce from '@/assets/iconInfoUser/Icon-UserList.svg';
import ClockClockwise from '@/assets/iconInfoUser/ClockClockwise.svg';
import ChalkboardTeacher from '@/assets/iconInfoUser/Icon-ChalkboardTeacher.svg';
import Note from '@/assets/iconInfoUser/Icon-Note.svg';
import Clock from '@/assets/iconInfoUser/education/Icon-Clock.svg';
import Exam from '@/assets/iconInfoUser/education/Icon-Exam.svg';
import GraduationCap from '@/assets/iconInfoUser/education/Icon-GraduationCap.svg';
export {
  Cake,
  EnvelopeSimple,
  GenderFemale,
  MapPin,
  PhoneCall,
  UserCircle,
  Introduce,
  ClockClockwise,
  ChalkboardTeacher,
  Note,
  Clock,
  Exam,
  GraduationCap,
};
