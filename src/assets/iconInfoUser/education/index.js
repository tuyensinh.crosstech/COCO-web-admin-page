import Exam from './Icon-Exam.svg';
import Clock from '../Icon-Clock.svg';
import ChalkboardTeacher from '../Icon-ChalkboardTeacher.svg';
import Note from './Icon-GraduationCap.svg';
export { Clock, ChalkboardTeacher, Note, Exam };
