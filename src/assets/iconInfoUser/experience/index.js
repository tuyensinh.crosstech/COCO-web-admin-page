import Briefcase from './Icon-Briefcase.svg';
import IdentificationBadge from './Icon-IdentificationBadge.svg';
import Clock from '../Icon-Clock.svg';
import ChalkboardTeacher from '../Icon-ChalkboardTeacher.svg';
import Note from './Icon-Note.svg';
export { Briefcase, IdentificationBadge, Clock, Note, ChalkboardTeacher };
