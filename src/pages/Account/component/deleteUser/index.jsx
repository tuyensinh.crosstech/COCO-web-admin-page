import { AccountService } from '@/services/Account';
import {
  showConfirmSuccess,
  showConfirmError,
} from '@/components/AccountModal/Modal';
import { useNavigate } from 'react-router-dom';
import routerLinks from '@/utils/router-links';
const onDeleteUser = async (id, setdata) => {
  // const navigate = useNavigate;
  try {
    const res = await AccountService.deleteAccount(id);
    if (res?.success) {
      showConfirmSuccess();
      setdata
        ? setdata((pre) => {
            return pre.filter((user) => user.id !== id);
          })
        : (document.getElementById('deleteUser').disabled = true);
      // : navigate(routerLinks('TÀI KHOẢN NGƯỜI DÙNG'));
    } else {
      showConfirmError();
    }
  } catch (err) {
    console.log('Error is:', err);
  }
};

export default onDeleteUser;
