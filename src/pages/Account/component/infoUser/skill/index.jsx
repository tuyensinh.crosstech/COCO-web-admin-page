import { Card, Row, Col } from 'antd';
import { UserList, SketchLogo } from '@/assets/iconInfoUser/skill';
import '../index.less';
import ListCard from '../component/listCard';
const Skill = (user_skill) => {
  const data = user_skill?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={SketchLogo} />
                {child?.skill}
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={UserList}></img>
                <p>{child?.describe}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Kỹ năng" />
    </>
  );
};
export default Skill;
