import { Card, List } from 'antd';
import '../../index.less';
const ListCard = ({ data, title }) => {
  return (
    <>
      <Card className="card-adress">
        <h5 className="titleCard">{title}</h5>
        <List
          size="default "
          dataSource={data}
          renderItem={(item) => <List.Item>{item}</List.Item>}
          pagination={{
            onChange: (page) => {},
            pageSize: 2,
          }}
        ></List>
      </Card>
    </>
  );
};
export default ListCard;
