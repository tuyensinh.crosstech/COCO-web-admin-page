import { useState, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { AccountService } from '@/services/Account';
import { ArrowSquareLeft } from '@/assets/index';
import { Button, Card, Row, Col, Tabs } from 'antd';
import {
  Cake,
  EnvelopeSimple,
  GenderFemale,
  MapPin,
  PhoneCall,
  UserCircle,
  Introduce,
  ClockClockwise,
  ChalkboardTeacher,
  Note,
} from '@/assets/iconInfoUser/index';
import './index.less';
import Exexperience from './experience';
import Education from './education';
import Prize from './prize';
import Skill from './skill';
import Certificate from './certificate';
import Extracurricular from './extracurricular';
import routerLinks from '@/utils/router-links';
import ModalConfim from '@/components/Modal';
import onLockAccount from '../lockUser';
import { useDispatch, useSelector } from 'react-redux';
import { setActiveHoppy } from '@/action/hoppy';
import {
  showApproveModal,
  showDeleteUserModal,
  showLockUserModal,
} from '@/components/AccountModal/Modal';
import onDeleteUser from '../deleteUser';
import confirmMentor from '../confirmMentor';
const Info = () => {
  const register = useLocation();
  const navigate = useNavigate();
  const { id } = useParams();
  const [info, setInfo] = useState({});
  const [loadding, setLoadding] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const InformSucec = useSelector((state) => state.hoppy.list);
  const dispatch = useDispatch();
  useEffect(() => {
    setLoadding(true);
    const infoUser = async () => {
      if (!id) return;
      try {
        const result = await AccountService.userDetails(id);
        if (result?.success) {
          setInfo(result?.user);
          setLoadding(false);
        }
      } catch (error) {}
    };
    infoUser();
  }, []);
  const bbb = (value) => {
    dispatch(setActiveHoppy(value));
  };
  return (
    <div className="info-user">
      <div className="info">
        <img
          onClick={() => {
            navigate(routerLinks('TÀI KHOẢN NGƯỜI DÙNG'));
          }}
          className="comeBack"
          src={ArrowSquareLeft}
        />
        <div className="title-info">Thông tin người dùng</div>
      </div>
      <Card className="card-adress">
        <Row>
          <Col lg={8} md={12} sm={24}>
            <div className="avatar">
              {info?.avatar ? (
                <img
                  style={{ maxWidth: '100%' }}
                  src={import.meta.env.VITE_REACT_APP_API_URL + info?.avatar}
                  alt="logo"
                />
              ) : info.gender === null || info?.gender ? (
                <img
                  style={{ maxWidth: '100%' }}
                  src="https://scr.vn/wp-content/uploads/2020/07/Avatar-Facebook-tr%E1%BA%AFng.jpg"
                  alt="logoMale"
                />
              ) : (
                <img
                  style={{ maxWidth: '100%' }}
                  src="https://scr.vn/wp-content/uploads/2020/07/%E1%BA%A2nh-%C4%91%E1%BA%A1i-di%E1%BB%87n-FB-m%E1%BA%B7c-%C4%91%E1%BB%8Bnh-n%E1%BB%AF.jpg"
                  alt="logoFemale"
                />
              )}
            </div>
          </Col>
          <Col lg={8} md={12} sm={24}>
            <div className="listInfo">
              <ul>
                <li className="titleCard">Thông tin liên hệ</li>
                <li>
                  <img src={UserCircle} />
                  {info?.fullname}
                </li>
                <li>
                  <img src={MapPin} /> {info?.ward?.name_vi}
                  {info?.ward?.name_vi ? ', ' : ''}
                  {info?.ward?.district?.name_vi}
                  {info?.ward?.district?.name_vi ? ', ' : ''}
                  {info?.ward?.district?.province?.name_vi}
                </li>
                <li>
                  <img src={GenderFemale} />
                  {info.gender === null || info?.gender ? 'Nam' : 'Nữ'}
                </li>
                <li>
                  <img src={Cake} /> {info?.birthday}
                </li>
                <li>
                  <img src={PhoneCall} /> {info?.phone}
                </li>
                <li>
                  <img src={EnvelopeSimple} />
                  {info?.email}
                </li>
              </ul>
            </div>
          </Col>
          <Col lg={8} md={12} sm={24}>
            <div>
              <p className="titleCard">GIỚI THIỆU BẢN THÂN</p>
              <div style={{ display: 'flex', marginTop: 10 }}>
                <img style={{ marginRight: '12px' }} src={Introduce} />
                <p>{info?.introduce_yourself}</p>
              </div>
            </div>
          </Col>
        </Row>
      </Card>
      {register.state ? (
        <Card className="card-adress">
          <Card className="card-alert">
            <Row>
              <img src={ClockClockwise} />
              <div>Tài khoản đang chờ phê duyệt để trở thành Mentor</div>
            </Row>
          </Card>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="headerRegister titleCard">LĨNH VỰC CỐ VẤN</div>
              {register?.state?.consulting_field?.map((item) => {
                return (
                  <>
                    <div className="contentRegister">
                      <img src={ChalkboardTeacher} className="iconRegister" />
                      <p key={item.id}>{item.name_vi}</p>
                    </div>
                  </>
                );
              })}
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="reason titleCard">
                LÝ DO MUỐN TRỞ THÀNH MENTOR
              </div>
              <div className="contentReason">
                <img src={Note}></img>
                <p>{register.state?.reason_register}</p>
              </div>
            </Col>
          </Row>
          <Row className="actionRegister">
            <Button
              onClick={() => {
                showApproveModal(true, () => {
                  confirmMentor(register.id, 'reject');
                });
              }}
              className="btn-refuse"
            >
              Từ chối
            </Button>
            <Button
              onClick={() => {
                showApproveModal(true, () => {
                  confirmMentor(register.id, 'approve');
                });
              }}
              className="btn-confirm"
            >
              Xác nhận
            </Button>
          </Row>
        </Card>
      ) : (
        <></>
      )}
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab="Kinh nghiệm làm việc" key="1">
          {Exexperience(info?.user_experience)}
        </Tabs.TabPane>

        <Tabs.TabPane tab="Quá trình học tập" key="2">
          {Education(info?.user_education)}
        </Tabs.TabPane>
        <Tabs.TabPane tab="Giải thưởng" key="3">
          {Prize(info?.user_prize)}
        </Tabs.TabPane>
        <Tabs.TabPane tab="Kỹ năng" key="4">
          {Skill(info?.user_skill)}
        </Tabs.TabPane>
        <Tabs.TabPane tab="Chứng chỉ" key="5">
          {Certificate(info?.user_certificate)}
        </Tabs.TabPane>
        <Tabs.TabPane tab="Hoạt động ngoại khóa" key="6">
          {Extracurricular(info?.user_extracurricular_activities)}
        </Tabs.TabPane>
      </Tabs>
      <Row style={{ justifyContent: 'flex-end' }}>
        <Button
          id="deleteUser"
          onClick={() => {
            showDeleteUserModal(() => onDeleteUser(info.id));
          }}
          style={{
            borderRadius: 8,
            background: '#dc2626',
            color: '#eee',
            width: 152,
          }}
        >
          Xóa tài khoản
        </Button>
        <Button
          // onClick={() => {
          //   setIsModalOpen(true);
          // }}
          onClick={() => {
            showLockUserModal(info.is_active, () => {
              onLockAccount(info);
            });
            info.is_active
              ? setInfo(() => {
                  const newInfo = { ...info };
                  newInfo.is_active = false;
                  return newInfo;
                })
              : setInfo(() => {
                  const newInfo = { ...info };
                  newInfo.is_active = true;
                  return newInfo;
                });
          }}
          style={{
            borderRadius: 8,
            background: '#EAB308',
            marginLeft: '24px',
            marginRight: '24px',
            width: 152,
          }}
        >
          {info.is_active ? 'Khóa tài khoản' : 'Mở tài khoản'}
        </Button>
        <div id="abc"></div>
        <Button
          style={{
            borderRadius: 8,
            background: '#FFFFFF',
            color: '#224922',
            width: 152,
          }}
        >
          chỉnh sửa
        </Button>
      </Row>
      {/* <ModalConfim
        open={isModalOpen}
        setOpen={setIsModalOpen}
        title="Khóa tài khoản"
        alertModal="Bạn có chắc chắn muốn xóa tài khoản Hoàng Vũ Ngọc Linh?"
        content="huan"
        button={['Hủy thao tác', 'Khóa tài khoản']}
        styleModal="warning "
        handle={() => onLockAccount(info, dispatch)}
      />
      <ModalConfim
        open={openInform}
        setOpen={bbb}
        title="Thành công!"
        alertModal="Phê duyệt yêu cầu thành công!"
        button={['Đóng']}
        styleModal="approve"
        // handle={}
      ></ModalConfim>
      <ModalConfim
        open={openInform}
        setOpen={bbb}
        title="Thành công!"
        alertModal="Phê duyệt yêu cầu thành công!"
        button={['Đóng']}
        styleModal="approve"
        // handle={}
      ></ModalConfim> */}
    </div>
  );
};
export default Info;
