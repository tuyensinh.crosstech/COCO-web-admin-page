import { Card, Row, Col } from 'antd';
import {
  BookBookmark,
  UserList,
  Clock,
} from '@/assets/iconInfoUser/certificate';
import '../index.less';
import ListCard from '../component/listCard';
const Certificate = (user_certificate) => {
  const data = user_certificate?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={BookBookmark} />
                {child?.certificate}
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Clock}></img>
                <p>{child?.received_date}</p>
              </div>
              <div className="contentReason">
                <img src={UserList}></img>
                <p>{child?.describe}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Chứng chỉ" />
    </>
  );
};
export default Certificate;
