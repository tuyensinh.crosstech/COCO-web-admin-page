import { Card, Row, Col } from 'antd';
import { Trophy, UserList, Clock } from '@/assets/iconInfoUser/prize';
import '../index.less';
import ListCard from '../component/listCard';
const Prize = (user_prize) => {
  const data = user_prize?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Trophy} />
                {child?.prize}
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Clock}></img>
                <p>{child?.received_date}</p>
              </div>
              <div className="contentReason">
                <img src={UserList}></img>
                <p>{child?.describe}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Giải thưởng" />
    </>
  );
};
export default Prize;
