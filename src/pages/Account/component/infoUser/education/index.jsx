import { Card, Row, Col } from 'antd';
import {
  Clock,
  ChalkboardTeacher,
  Note,
  Exam,
} from '@/assets/iconInfoUser/education';
import '../index.less';
import ListCard from '../component/listCard';
const Education = (user_education) => {
  const data = user_education?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Note} />
                {child?.school_name}
              </div>
              <div className="contentReason">
                <img src={ChalkboardTeacher} />
                <p>{child?.specialized_major}</p>
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Clock}></img>
                {child?.until_now ? (
                  <p>{child?.start_time}- Hiện tại.</p>
                ) : (
                  <p>
                    {child?.start_time}-{child?.end_time}
                  </p>
                )}
              </div>
              <div className="contentReason">
                <img src={Exam} />
                <p>{child?.scores}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Quá trình học tập" />
    </>
  );
};
export default Education;
