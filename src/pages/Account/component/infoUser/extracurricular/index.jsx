import { Card, Row, Col } from 'antd';
import {
  ChalkboardTeacher,
  Note,
  Clock,
  UsersThree,
} from '@/assets/iconInfoUser/Extracurricular';
import '../index.less';
import ListCard from '../component/listCard';
const Extracurricular = (user_experience) => {
  const data = user_experience?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={UsersThree} />
                {child?.activity}
              </div>
              <div className="contentReason">
                <img src={ChalkboardTeacher} />
                <p>{child?.position}</p>
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Clock}></img>
                {child?.until_now ? (
                  <p>{child?.start_time}- Hiện tại.</p>
                ) : (
                  <p>
                    {child?.start_time}-{child?.end_time}
                  </p>
                )}
              </div>
              <div className="contentReason">
                <img src={Note}></img>
                <p>{child?.describe}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Hoạt động ngoại khóa" />
    </>
  );
};
export default Extracurricular;
