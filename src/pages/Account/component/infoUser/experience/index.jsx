import { Card, Row, Col, Pagination, List } from 'antd';
import {
  Briefcase,
  IdentificationBadge,
  Clock,
  Note,
  ChalkboardTeacher,
} from '@/assets/iconInfoUser/experience/index';
import '../index.less';
import ListCard from '../component/listCard';
const Exexperience = (user_experience) => {
  const data = user_experience?.map((child) => {
    return (
      <Card className="cardDetail">
        <div key={child.id}>
          <Row>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Briefcase} />
                {child?.company_name}
              </div>
              <div className="contentReason">
                <img src={ChalkboardTeacher} />
                <p>{child?.consulting_field.name_vi}</p>
              </div>
              <div className="contentReason">
                <img src={IdentificationBadge} />
                <p>{child?.working_position}</p>
              </div>
            </Col>
            <Col xl={8} lg={8} md={12} sm={24}>
              <div className="contentReason">
                <img src={Clock}></img>
                {child?.until_now ? (
                  <p>{child?.start_time}- Hiện tại.</p>
                ) : (
                  <p>
                    {child?.start_time}-{child?.end_time}
                  </p>
                )}
              </div>
              <div className="contentReason">
                <img src={Note}></img>
                <p>{child?.job_description}</p>
              </div>
            </Col>
          </Row>
        </div>
      </Card>
    );
  });
  return (
    <>
      <ListCard data={data} title="Kinh nghiệm làm việc" />
    </>
  );
};
export default Exexperience;
