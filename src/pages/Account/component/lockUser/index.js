import {
  showConfirmSuccess,
  showConfirmError,
} from '@/components/AccountModal/Modal';
import { AccountService } from '@/services/Account';
const lock = async (info, action, setdata) => {
  try {
    const result = await AccountService.lockAccount(info.id, action);
    if (result?.success) {
      showConfirmSuccess();
      setdata
        ? setdata((data) => {
            console.log('adsd');
            const newAccount = [...data];
            const index = data.findIndex(function (data) {
              return data.id === info.id;
            });
            if (newAccount[index].is_active) {
              newAccount[index].is_active = false;
            } else {
              newAccount[index].is_active = true;
            }
            return newAccount;
          })
        : '';
      // dispatch(setActiveHoppy(true));
    } else {
      // dispatch(setActiveHoppy(true));
      showConfirmError();
    }
  } catch (error) {
    console.log(error);
  }
};
const onLockAccount = (info, setdata) => {
  setdata((data) => {
    const newAccount = [...data];
    const index = data.findIndex(function (data) {
      return data.id === info.id;
    });
    if (newAccount[index].is_active) {
      lock(info, 'lock');
      console.log('a');
      newAccount[index].is_active = false;
    } else {
      lock(info, 'unlock');
      newAccount[index].is_active = true;
    }
    return newAccount;
  });
  if (info.is_active) {
    lock(info, 'lock', setdata);
  } else {
    lock(info, 'unlock', setdata);
  }
};
export default onLockAccount;
