import { AccountService } from '@/services/Account';
import {
  showConfirmSuccess,
  showConfirmError,
} from '@/components/AccountModal/Modal';
const confirmMentor = async (id, action, setDataTable, dataTable) => {
  try {
    const result = await AccountService.confirmMentor(id, action);
    if (result?.success) {
      showConfirmSuccess();
      setDataTable
        ? setDataTable(
            dataTable.filter((newdata) => {
              return newdata.registration.id !== id;
            })
          )
        : '';
    } else {
      showConfirmError();
    }
  } catch (error) {
    showConfirmError();
    console.log(error);
  }
};
export default confirmMentor;
