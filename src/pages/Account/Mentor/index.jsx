// import Table from '../../component/table';

import { useEffect, useState } from 'react';

import gettotalUserRole from '../component/totalUser';
import getdata from '../component/dataUser';
import { Table, Pagination, Card } from 'antd';
import { columns } from './columns';
const Mentor = () => {
  const [data, setdata] = useState([]);
  const [total, setTotal] = useState(1);

  useEffect(() => {
    gettotalUserRole('4', setTotal);
    getdata(4, 1, 10, setdata);
  }, []);

  return (
    <>
      <Table columns={columns(setdata)} dataSource={data} pagination={false} />
      <Pagination
        total={total}
        showTotal={(total, range) => {
          return `${range[0]}-${range[1]} of ${total} items`;
        }}
        defaultPageSize={10}
        defaultCurrent={1}
        onChange={(page, pageSize) => {
          getdata(4, page, pageSize, setdata);
        }}
      />
    </>
  );
};
export default Mentor;
