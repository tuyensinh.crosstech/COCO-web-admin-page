import {
  DeleteOutlined,
  EyeOutlined,
  LockOutlined,
  UnlockOutlined,
} from '@ant-design/icons';
import { Divider } from 'antd';
import { useNavigate } from 'react-router-dom';
import routerLinks from '@/utils/router-links';
import {
  showDeleteUserModal,
  showLockUserModal,
} from '@/components/AccountModal/Modal';
import onLockAccount from '../component/lockUser';
import onDeleteUser from '../component/deleteUser';
export const columns = (setData) => {
  const navigate = useNavigate();
  return [
    {
      title: 'HỌ TÊN',
      key: '1',
      dataIndex: 'fullname',
    },

    {
      title: 'EMAIL',
      key: '2',
      dataIndex: 'email',
    },
    {
      title: 'SỐ ĐIỆN THOẠI',
      key: '3',
      dataIndex: 'phone',
    },
    {
      title: 'LĨNH VỰC',
      key: '4',
      dataIndex: 'field',
    },
    {
      title: 'NGÀY HOẠT ĐỘNG',
      key: '5',
      dataIndex: 'activeDay',
    },
    {
      title: 'TRẠNG THÁI',
      key: 'action',
      render: (payload) => {
        return <div>{payload.is_active ? 'Đang hoạt động' : 'Vô hiệu'}</div>;
      },
    },
    {
      title: 'HOẠT ĐỘNG',
      key: '6',
      render: (_, info) => (
        // <Space size="middle">
        <>
          <EyeOutlined
            onClick={() => {
              navigate(`${info.id}`, { replace: true });
            }}
          />
          <Divider type="vertical" />
          <span
            onClick={() =>
              showLockUserModal(info.is_active, () => {
                onLockAccount(info, setData);
              })
            }
          >
            {info.is_active ? <LockOutlined /> : <UnlockOutlined />}
          </span>
          <Divider type="vertical" />
          <DeleteOutlined
            style={{ color: 'red' }}
            onClick={() => {
              showDeleteUserModal(() => onDeleteUser(info.id, setData));
            }}
          />
        </>
        // </Space>
      ),
    },
  ];
};
