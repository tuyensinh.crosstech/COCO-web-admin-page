import { Search } from '@/components';

import Mentor from './mentor';
import Mentee from './mentee';
import MentorPending from './mentorPending';
import { Tabs, Card } from 'antd';
function Page() {
  return (
    <>
      <Card>
        <div>
          <h2 style={{ fontSize: 40 }}>QUẢN LÝ TÀI KHOẢN NGƯỜI DÙNG</h2>
          <Search />
          <Tabs defaultActiveKey="1">
            <Tabs.TabPane tab="Mentor" key="1">
              <Mentor />
            </Tabs.TabPane>
            <Tabs.TabPane tab="Mentor Pendding" key="2">
              <MentorPending />
            </Tabs.TabPane>
            <Tabs.TabPane tab="Mentee" key="3">
              <Mentee />
            </Tabs.TabPane>
          </Tabs>
        </div>
      </Card>
    </>
  );
}

export default Page;
