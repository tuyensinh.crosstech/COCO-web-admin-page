import { useEffect, useState } from 'react';

import gettotalUserRole from '../component/totalUser';
import { getdataMentorPanding } from '../component/dataUser';
import { Table, Pagination } from 'antd';
import { columns } from './columns';

const MentorPending = () => {
  const [data, setdata] = useState([]);
  const [total, setTotal] = useState(1);
  // const [dataTable, setDataTable] = useState([]);
  let uniqueId = 1;
  useEffect(() => {
    gettotalUserRole('4', setTotal);
    getdataMentorPanding(1, 10, setdata);
  }, []);
  console.log(data);
  return (
    <>
      {/* <con></con> */}
      <Table
        columns={columns(setdata, data)}
        dataSource={data}
        pagination={false}
        rowKey={(record) => {
          if (!record.__uniqueId) record.__uniqueId = ++uniqueId;
          return record.__uniqueId;
        }}
      />
      <Pagination
        total={total}
        showTotal={(total, range) => {
          return `${range[0]}-${range[1]} of ${total} items`;
        }}
        defaultPageSize={10}
        defaultCurrent={1}
        onChange={(page, pageSize) => {
          getdata(4, page, pageSize, setdata);
        }}
      />
    </>
  );
};
export default MentorPending;
