import { AccountService } from '@/services/Account';
import { Input, Tabs } from 'antd';
import { useEffect, useMemo, useState } from 'react';
import './scrollbar.less';
import UserNotificationItem from './UserNotificationItem';

const { Search } = Input;

const ListusersNotificated = ({ selectedUsers, setSelectedUsers }) => {
  const [users, setUsers] = useState([]);
  const [queryUsers, setQueryUsers] = useState('');
  const [querySearchUsers, setQuerySearchUsers] = useState('');

  const filterUsers = useMemo(() => {
    const searchCondition = (user) => {
      return !!querySearchUsers
        ? user?.fullname.toLowerCase().includes(querySearchUsers.toLowerCase())
        : true;
    };
    if (queryUsers === 'selected') {
      return users.filter(
        (user) => selectedUsers[user?.id] && searchCondition(user)
      );
    }
    if (!queryUsers) {
      return users.filter((user) => searchCondition(user));
    }
    const query = queryUsers === 'mentor' ? 4 : 5;
    return users.filter(
      (user) => user.role_id === query && searchCondition(user)
    );
  }, [users, queryUsers, querySearchUsers]);

  const fetchList = async () => {
    const response = await AccountService.getAllUser();

    if (response?.success) {
      const isMentor = 4;
      const isMentee = 5;
      setUsers(
        response?.users?.filter(
          (user) => user?.role_id === isMentor || user?.role_id === isMentee
        )
      );
    }
  };

  useEffect(() => {
    fetchList();
  }, []);

  const onChange = (key) => {
    setQueryUsers(key === 'all' ? undefined : key);
  };

  return (
    <>
      <Search
        disabled={false}
        style={{ margin: '5px 0px' }}
        placeholder={`Search Name`}
        onSearch={(value) => setQuerySearchUsers(value)}
        allowClear
      />
      <Tabs
        defaultActiveKey="all"
        onChange={onChange}
        items={[
          {
            label: `Đã Chọn`,
            key: 'selected',
            children: (
              <UserNotificationItem
                users={filterUsers}
                selectedUsers={selectedUsers}
                setSelectedUsers={setSelectedUsers}
                type="selected"
              />
            ),
          },
          {
            label: `Tất Cả`,
            key: 'all',
            children: (
              <UserNotificationItem
                users={filterUsers}
                selectedUsers={selectedUsers}
                setSelectedUsers={setSelectedUsers}
                type="all"
              />
            ),
          },
          {
            label: `Mentor`,
            key: 'mentor',
            children: (
              <UserNotificationItem
                users={filterUsers}
                selectedUsers={selectedUsers}
                setSelectedUsers={setSelectedUsers}
                type="mentor"
              />
            ),
          },
          {
            label: `Mentee`,
            key: 'mentee',
            children: (
              <UserNotificationItem
                users={filterUsers}
                selectedUsers={selectedUsers}
                setSelectedUsers={setSelectedUsers}
                type="mentee"
              />
            ),
          },
        ]}
      />
    </>
  );
};
export default ListusersNotificated;
