import { UserOutlined } from '@ant-design/icons';
import { Avatar, Checkbox, Divider, Form, List, Skeleton } from 'antd';
import { useRef, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import './scrollbar.less';

const UserNotificationItem = ({
  users,
  selectedUsers,
  setSelectedUsers,
  type,
}) => {
  const ref = useRef(null);
  const len = users.length;

  const handleCheck = (e, id, role_id) => {
    const countRole = role_id === 4 ? 'mentorCount' : 'menteeCount';
    if (e.target.checked) {
      setSelectedUsers((pre) => {
        return {
          ...pre,
          [id]: role_id,
          count: pre.count + 1,
          [countRole]: pre[countRole] + 1,
        };
      });
    } else {
      setSelectedUsers((pre) => {
        const obj = { ...pre };
        delete obj[id];
        return {
          ...obj,
          count: pre.count - 1,
          [countRole]: obj[countRole] - 1,
        };
      });
    }
  };

  const onHandleCheckAll = (e) => {
    if (e.target.checked) {
      users.forEach((user) => {
        const countRole = user?.role_id === 4 ? 'mentorCount' : 'menteeCount';
        setSelectedUsers((pre) => {
          const countNumber = pre[user?.id] ? 0 : 1;
          return {
            ...pre,
            [user?.id]: user?.role_id,
            count: pre.count + countNumber,
            [countRole]: pre[countRole] + countNumber,
          };
        });
      });
    } else
      setSelectedUsers((pre) => {
        let temp = { ...pre };
        users.forEach((user) => {
          const countRole = user?.role_id === 4 ? 'mentorCount' : 'menteeCount';
          delete temp[user.id];
          temp = {
            ...temp,
            count: temp.count - 1,
            [countRole]: temp[countRole] - 1,
          };
        });
        return temp;
      });
  };

  const isCheckedUser = () => {
    if (selectedUsers.count > 0) {
      return Promise.resolve();
    }
    return Promise.reject(new Error('Chưa người dùng nào được chọn'));
  };

  const form = Form.useFormInstance();

  return (
    <>
      <Form.Item
        name="listUser"
        rules={[
          {
            validator: form.isFieldTouched('listUser')
              ? isCheckedUser
              : undefined,
          },
        ]}
      >
        <div
          id="scrollableDiv"
          className="user-notification-scroll mb-2"
          style={{
            borderColor:
              form.isFieldTouched('listUser') && selectedUsers?.count <= 0
                ? '#FF4D4F'
                : '#8c8c8c59',
            transitionDelay: '0.3',
          }}
        >
          <InfiniteScroll
            dataLength={users?.length}
            // next={loadMoreData}
            // hasMore={users.length < 50}
            loader={
              <Skeleton
                avatar
                paragraph={{
                  rows: 5,
                }}
                active
              />
            }
            endMessage={<Divider plain>All users is loaded</Divider>}
            scrollableTarget="scrollableDiv"
          >
            {type !== 'selected' && (
              <div
                style={{
                  witdh: ref?.current?.clientWidth
                    ? `${ref?.current?.clientWidth}`
                    : 'auto',
                  padding: '12px',
                  display: 'flex',
                  justifyContent: 'end',
                }}
              >
                <Checkbox
                  style={{ marginRight: '20px' }}
                  indeterminate={
                    type === 'all'
                      ? selectedUsers.count > 0 && selectedUsers.count < len
                      : selectedUsers[`${type}Count`] > 0 &&
                        selectedUsers[`${type}Count`] < len
                  }
                  checked={
                    type === 'all'
                      ? selectedUsers.count === len && len > 0
                      : selectedUsers[`${type}Count`] === len && len > 0
                  }
                  onClick={(e) => onHandleCheckAll(e)}
                />
              </div>
            )}
            <List
              dataSource={users}
              renderItem={(item) => (
                <List.Item key={item?.id} ref={ref}>
                  <List.Item.Meta
                    avatar={
                      <Avatar
                        // size="large"
                        alt="avatar"
                        style={{
                          backgroundColor: '#2f672f',
                          color: '#fff',
                        }}
                        icon={<UserOutlined />}
                      />
                    }
                    title={item?.fullname}
                    description={item?.role_id === 4 ? 'mentor' : 'mentee'}
                  />
                  <div style={{ marginRight: '20px' }}>
                    <Checkbox
                      checked={selectedUsers[`${item?.id}`]}
                      onClick={(e) => handleCheck(e, item?.id, item?.role_id)}
                    />
                  </div>
                </List.Item>
              )}
            />
          </InfiniteScroll>
        </div>
      </Form.Item>
    </>
  );
};

export default UserNotificationItem;
