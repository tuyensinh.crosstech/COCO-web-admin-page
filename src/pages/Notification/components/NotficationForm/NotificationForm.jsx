import { NotificationService } from '@/services/notificationsService';
import { Button, Col, DatePicker, Form, Input, Radio, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import moment from 'moment';
import { useEffect, useRef, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import './customDataPicker.less';
import ListUserNotificated from './ListUserNotificated/ListUserNotificated';
const layout = {
  labelCol: {
    span: 2,
  },
  wrapperCol: {
    span: 10,
  },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} là ô bắt buộc',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
};
/* eslint-enable no-template-curly-in-string */

const App = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [isSchedule, setIsSchedule] = useState(0);
  const [initValue, setInitValue] = useState({});
  const [selectedUsers, setSelectedUsers] = useState({
    menteeCount: 0,
    mentorCount: 0,
    count: 0,
  });

  const [form] = Form.useForm();

  const location = useLocation();
  const isMounted = useRef(true);

  const disabled =
    location.pathname.includes('edit') || location.pathname.includes('add')
      ? false
      : true;

  useEffect(() => {
    if (isMounted.current) {
      isMounted.current = false;
      if (id) {
        const request = async () => {
          const response = await NotificationService.getNotificationDetail(id);
          try {
            if (response?.success) {
              setIsSchedule(
                response?.notification?.is_schedule ? 'true' : 'false'
              );

              setInitValue(response?.notification);

              if (!response?.notification?.is_schedule) return;

              for (const user of response?.notification?.user) {
                const countRole =
                  user.role.id === 4 ? 'mentorCount' : 'menteeCount';
                setSelectedUsers((pre) => {
                  return {
                    ...pre,
                    [user.id]: user.role.id,
                    count: pre.count + 1,
                    [countRole]: pre[countRole] + 1,
                  };
                });
              }
            }
          } catch (error) {}
        };
        request();
        return;
      }
      setInitValue();
    }
  }, []);

  useEffect(() => {
    form.resetFields();
  }, [initValue]);

  const onHandleSchedule = (e) => {
    setIsSchedule(e.target.value);
  };

  const onCreate = async (values) => {
    let listUsers = Object.keys(selectedUsers);
    listUsers.splice(listUsers.length - 3);

    const notificationTemplateData = {
      title: values.title,
      description: values.description,
      ...{ users: listUsers },
    };

    const schedule = {
      isSchedule: values.schedule === 0 ? false : true,
      date:
        values.schedule === 1
          ? new Date(values?.scheduleDate?._d).getTime().toString()
          : '',
    };

    const res = await NotificationService.createNotification(
      notificationTemplateData,
      schedule
    );
    try {
      if (res?.success) {
        swal({
          title: 'Thông Báo đã được tạo',
          text: 'Thông Báo đã được tạo thành công!',
          icon: 'success',
          button: 'Đồng Ý',
        });
        navigate('/notification', { replace: true });
      }
    } catch (error) {
      swal({
        title: 'Tạo thông báo không thành công',
        text: 'Thông báo đã không được tạo thành công!',
        icon: 'error',
        button: 'Đồng Ý',
      });
      throw error;
    }
  };

  const onUpdate = async (values) => {
    let listUsers = Object.keys(selectedUsers);
    listUsers.splice(listUsers.length - 3);

    const notificationTemplateData = {
      title: values.title,
      description: values.description,
      ...{ usersID: listUsers },
      date:
        values.schedule === 1
          ? new Date(values?.scheduleDate?._d).getTime()
          : '',
    };

    const res = await NotificationService.updateNotification(
      notificationTemplateData,
      id,
      values.schedule ? 0 : 1
    );
    try {
      if (res?.success) {
        swal({
          title: 'Thông Báo đã được cập nhật',
          text: 'Thông Báo đã được cập nhật thành công!',
          icon: 'success',
          button: 'Đồng Ý',
        });
        navigate('/notification', { replace: true });
      }
    } catch (error) {
      swal({
        title: 'Cập nhật thông báo không thành công',
        text: 'Thông báo đã không được cập nhật thành công!',
        icon: 'error',
        button: 'Đồng Ý',
      });
      throw error;
    }
  };

  const onFinish = async (values) => {
    if (location.pathname.includes('add')) {
      onCreate(values);
      return;
    }
    onUpdate(values);
  };

  return (
    <Form
      form={form}
      initialValues={{
        title: initValue?.title,
        description: initValue?.description,
        schedule: initValue?.is_schedule ? 1 : 0,
        scheduleDate: moment(initValue?.schedule_time),
      }}
      disabled={disabled}
      style={{ padding: '30px', background: 'white' }}
      layout="vertical"
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <Row>
        <Col span={12}>
          <Form.Item
            name="title"
            label="Tiêu Đề"
            wrapperCol={{ span: 10 }}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="description"
            label="Nội Dung"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input.TextArea rows={6} />
          </Form.Item>
          <Form.Item
            name="schedule"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Radio.Group
              style={{ width: '100%' }}
              value={isSchedule}
              onChange={(e) => onHandleSchedule(e)}
            >
              <Row>
                <Col span={8}>
                  <Radio value={0} checked={!isSchedule}>
                    Ngay Lập Tức
                  </Radio>
                </Col>
                <Col span={16}>
                  <Radio value={1} checked={isSchedule}>
                    Lên Lịch
                  </Radio>
                </Col>
              </Row>
            </Radio.Group>
          </Form.Item>
          {!!isSchedule && (
            <Form.Item
              label="Ngày Lên Lịch"
              name="scheduleDate"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <DatePicker
                className="custom-notification-datepicker"
                format="YYYY-MM-DD HH:mm"
                showTime={{
                  defaultValue: moment('00:00', 'HH:mm'),
                  format: 'HH:mm',
                }}
              />
            </Form.Item>
          )}
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button
              disabled={false}
              type="default"
              onClick={() => navigate('/notification', { replace: true })}
            >
              Cancel
            </Button>
          </Form.Item>
        </Col>
        <Col span={12} style={{ padding: '0px 20px' }}>
          <Title level={5}>Danh Sach</Title>
          <ListUserNotificated
            selectedUsers={selectedUsers}
            setSelectedUsers={setSelectedUsers}
          />
        </Col>
      </Row>
    </Form>
  );
};
export default App;
