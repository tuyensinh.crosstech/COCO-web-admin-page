import useTable from '@/hooks/useTable';
import { NotificationService } from '@/services/notificationsService';
import { DatePicker, Select } from 'antd';

import { Button, Col, Form, Input, Row } from 'antd';
import { useMemo, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const { Search } = Input;
const { RangePicker } = DatePicker;

const ColProps = {
  // xs: 24,
  // sm: 12,
  style: {
    marginBottom: 16,
  },
};

const TwoColProps = {
  ...ColProps,
  // xl: 96,
};

const AdvancedSearchForm = ({ userCreated, fetchRows, params, onReset }) => {
  const location = useLocation();
  const navigate = useNavigate();

  const [expand, setExpand] = useState(false);
  const [form] = Form.useForm();

  const is_sent = [
    {
      value: 0,
      label: 'Chưa Gửi',
    },
    {
      value: 1,
      label: 'Đã Gửi',
    },
  ];

  const optionUserCreated = useMemo(() => {
    return userCreated?.map((user) => {
      return {
        value: user.id,
        label: user.name,
      };
    });
  }, [JSON.stringify(userCreated)]);

  const onCreate = () => {
    navigate(`${location.pathname}/add`);
  };

  const getFields = () => {
    return (
      <>
        <Col span={20}>
          <Row gutter={16}>
            <Col {...ColProps} span={5}>
              <Form.Item name="title">
                <Search
                  allowClear
                  placeholder={`Tìm kiếm thông báo (theo tiêu đề)`}
                />
              </Form.Item>
            </Col>
            <Col {...ColProps} span={5}>
              <Form.Item name="user_id">
                <Select
                  allowClear
                  style={{ width: '100%' }}
                  options={optionUserCreated}
                  placeholder={`Người Gửi`}
                />
              </Form.Item>
            </Col>
            <Col {...ColProps} span={5}>
              <Form.Item name="is_sent">
                <Select
                  allowClear
                  style={{ width: '100%' }}
                  options={is_sent}
                  placeholder={`Tình Trạng`}
                />
              </Form.Item>
            </Col>
            <Col {...ColProps} span={9} id="createTimeRangePicker">
              <div className="flex justify-between">
                <div className="flex w-auto leading-7 mr-3 justify-between overflow-hidden text-sm">
                  <span>Create Date</span>
                </div>
                <div className="flex-1">
                  <Form.Item name="notificateTimeRange">
                    <RangePicker allowClear style={{ width: '100%' }} />
                  </Form.Item>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
        <Col {...ColProps} span={4}>
          <Button type="primary" htmlType="submit" className="margin-right">
            Search
          </Button>
          <Button
            onClick={() => {
              form.resetFields();
              onReset();
            }}
          >
            Reset
          </Button>
        </Col>
        <Col {...TwoColProps}>
          <Button type="ghost" onClick={onCreate}>
            Create
          </Button>
        </Col>
      </>
    );
  };

  const onFinish = async (values) => {
    fetchRows({
      page: params.page,
      amount: params.amount,
      search: `title=${values?.title || ''}&user_id=${
        values?.user_id || ''
      }&is_sent=${values?.is_sent || ''}&startDate=${
        values?.notificateTimeRange
          ? (values?.notificateTimeRange[0]?._d).getTime().toString()
          : ''
      }&endDate=${
        values?.notificateTimeRange
          ? (values?.notificateTimeRange[0]?._d).getTime().toString()
          : ''
      }`,
    });
  };
  return (
    <Form
      form={form}
      name="advanced_search"
      className="ant-advanced-search-form"
      onFinish={onFinish}
    >
      <Row gutter={24}>{getFields()}</Row>
    </Form>
  );
};

export default AdvancedSearchForm;
