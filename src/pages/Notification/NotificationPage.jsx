import { DeleteOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { Divider, Pagination, Table } from 'antd';

import useTable from '@/hooks/useTable';
import { NotificationService } from '@/services/notificationsService';
import { useEffect, useState } from 'react';
import AdvancedSearchForm from './components/AdvanceSearchForm/AdvanceSearchForm';

let uniqueId = 1;

const NotificationPage = () => {
  const {
    tableData,
    fetchRows,
    params,
    onPageChange,
    onPageSizeChange,
    onEdit,
    onView,
    onDelete,
    onReset,
  } = useTable({
    getByPage: NotificationService.getNotificationByPage,
    deleteNotification: NotificationService.deleteNotification,
    getCountPage: NotificationService.getNotificationCountPage,
  });

  const [user, setUser] = useState({});
  const [userCreated, setUserCreated] = useState();
  const [status, setStatus] = useState();

  const columns = [
    {
      title: 'Tiêu Đề',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Nội Dung',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Người Gửi',
      dataIndex: 'created_user_id',
      key: 'created_user_id',
      render: (_, record) => (
        <>
          {
            userCreated?.find((user) => user.id === record.created_user_id)
              ?.name
          }
        </>
      ),
    },
    {
      title: 'Ngày Gửi',
      dataIndex: 'schedule_time',
      key: 'schedule_time',
    },
    {
      title: 'Tình Trạng',
      dataIndex: 'is_sent',
      key: 'is_sent',
      render: (_, record) => <>{record?.is_sent ? 'Đã Gửi' : 'Đã Lên Lịch'}</>,
    },
    {
      title: 'Hành Động',
      dataIndex: 'action',
      render: (_, record) => (
        <>
          <a onClick={() => onView(record.id)}>
            <EyeOutlined />
          </a>
          <Divider type="vertical" />
          <a onClick={() => onEdit(record.id)}>
            <EditOutlined />
          </a>
          <Divider type="vertical" />
          <a onClick={() => onDelete(record.id, record.title)}>
            <DeleteOutlined />
          </a>
        </>
      ),
    },
  ];

  useEffect(() => {
    fetchRows(params);

    const request = async () => {
      const res = await NotificationService.getListUsersCreatedNotification();
      try {
        if (res?.success) {
          setUserCreated(res?.users);
        }
      } catch (err) {
        throw err;
      }
    };
    request();
  }, [params]);

  return (
    <div style={{ padding: '30px', background: 'white' }}>
      <AdvancedSearchForm
        userCreated={userCreated}
        fetchRows={fetchRows}
        params={params}
        onReset={onReset}
      />
      <Table
        rowKey={(record) => {
          if (!record.__uniqueId) record.__uniqueId = ++uniqueId;
          return record.__uniqueId;
        }}
        columns={columns}
        dataSource={tableData?.data}
        pagination={false}
        style={{ marginBottom: '20px' }}
      />
      {tableData?.total > 10 ? (
        <Pagination
          className="ant-table-pagination ant-table-pagination-right"
          total={tableData?.total}
          // showSizeChanger
          showQuickJumper
          current={params?.page}
          showSizeChanger={false}
          onChange={(page) => {
            onPageChange({ page: page });
          }}
          // onShowSizeChange={(size) => {
          //   onPageSizeChange({ size: size });
          // }}
        />
      ) : null}
    </div>
  );
};
export default NotificationPage;
