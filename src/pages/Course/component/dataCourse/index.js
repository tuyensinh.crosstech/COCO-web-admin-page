import { CourseService } from '@/services/Course';
const formatData = (data) => {
  for (let i = 0; i < data.length; i++) {
    data[i].key = i;
  }
  return data;
};
const getDataCourse = async (page, limit, setdata) => {
  try {
    const response = await CourseService.getCourse(page, limit);
    if (response?.success) {
      setdata(formatData(response?.courses));
    }
  } catch (err) {
    console.log('Error is:', err);
    // setLoading(false);
  }
};
export { getDataCourse };
