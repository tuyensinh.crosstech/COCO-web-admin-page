import { CourseService } from '@/services/Course';
import {
  showConfirmSuccess,
  showConfirmError,
} from '@/components/AccountModal/Modal';
const onDeleteCourse = async (id, setdata) => {
  try {
    const res = await CourseService.deleteCourse(id);
    if (res?.success) {
      showConfirmSuccess();
      setdata((pre) => {
        return pre.filter((course) => course.id !== id);
      });
    } else {
      showConfirmError();
    }
  } catch (err) {
    console.log('Error is:', err);
  }
};

export default onDeleteCourse;
