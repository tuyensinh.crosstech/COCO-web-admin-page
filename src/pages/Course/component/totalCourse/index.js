import { CourseService } from '@/services/Course';
const gettotalCourse = async (setTotal) => {
  try {
    const response = await CourseService.totalCourse();
    if (response?.success) {
      setTotal(response?.count);
    }
  } catch (err) {
    console.log('Error is:', err);
  }
};

export default gettotalCourse;
