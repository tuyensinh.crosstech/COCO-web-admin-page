import React, { useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import {
  Form,
  Input,
  Button,
  DatePicker,
  TreeSelect,
  Checkbox,
  Upload,
  Card,
} from 'antd';
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const FormDisabledDemo = () => {
  const FIELDS = ['CNTT', 'KT', 'KI THUAT'];
  const onFinish = (values) => {
    values.start_time = values.time[0]._d;
    values.end_time = values.time[1]._d;
    delete values.time;
  };
  return (
    <>
      <Card>
        <Form onFinish={onFinish} layout="horizontal">
          <Form.Item label="Tên khóa học" name="name_vi">
            <Input />
          </Form.Item>
          <Form.Item label="Tên tên trung tâm" name="center_name">
            <Input />
          </Form.Item>
          <Form.Item label="Link khóa học" name="registration_link">
            <Input />
          </Form.Item>
          <Form.Item
            label="Banner Khóa học và Thumbnail"
            valuePropName="fileList"
          >
            <Upload listType="picture-card">
              <div>
                <PlusOutlined />
                <div
                  style={{
                    marginTop: 8,
                  }}
                >
                  Upload
                </div>
              </div>
            </Upload>
          </Form.Item>
          <Form.Item label="Lĩnh vực" name="field">
            <TreeSelect
              treeData={FIELDS.map((field) => {
                return { title: field, value: field };
              })}
            />
          </Form.Item>
          <Form.Item label="Tên giảng viên" name="teacher_name">
            <Input />
          </Form.Item>
          <Form.Item label="Học phí" name="tuition">
            <Input />
          </Form.Item>
          <Form.Item
            label="Khóa học Miễn phí"
            name="free_tuition"
            valuePropName="checked"
          >
            <Checkbox />
          </Form.Item>
          <Form.Item label="Thời gian khóa học" name="time">
            <RangePicker />
          </Form.Item>
          <Form.Item label="mô tả" name="detail_vi">
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item label="Button">
            <Button type="primary" htmlType="submit">
              Button
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
};
export default () => <FormDisabledDemo />;
