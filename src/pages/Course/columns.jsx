import {
  DeleteOutlined,
  EyeOutlined,
  LockOutlined,
  UnlockOutlined,
} from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import routerLinks from '@/utils/router-links';
import {
  showDeleteUserModal,
  showLockUserModal,
} from '@/components/AccountModal/Modal';

export const columns = (onDelete, setData) => {
  const navigate = useNavigate();
  return [
    {
      title: 'Khóa học',
      key: '1',
      dataIndex: 'name_vi',
    },

    {
      title: 'Lĩnh vực',
      key: '2',
      dataIndex: 'major',
    },
    {
      title: 'Giảng viên',
      key: '3',
      dataIndex: 'teacher_name',
    },
    {
      title: 'Học phí',
      key: '4',
      dataIndex: 'tuition',
    },
    {
      title: 'Ngày bắt đầu',
      key: '5',
      dataIndex: 'start_time',
    },
    {
      title: 'Ngày kết thúc',
      key: 'action',
      dataIndex: 'end_time',
      // render: (payload) => {
      //   return <div>{payload.is_active ? 'Đang hoạt động' : 'Vô hiệu'}</div>;
      // },
    },
    {
      title: 'HOẠT ĐỘNG',
      key: '6',
      render: (_, info) => (
        // <Space size="middle">
        <>
          <EyeOutlined
            onClick={() => {
              navigate(`${info.id}`, { replace: true });
            }}
          />

          <DeleteOutlined
            onClick={() => {
              showDeleteUserModal(() => onDelete(info.id, setData));
            }}
          />
        </>
      ),
    },
  ];
};
