import { useEffect, useState } from 'react';
import { Search } from '@/components';
import { useNavigate } from 'react-router-dom';
import routerLinks from '@/utils/router-links';
import { Table, Pagination, Card, Button } from 'antd';
import { columns } from './columns';
import { getDataCourse } from './component/dataCourse';
import gettotalCourse from './component/totalCourse';
import onDeleteCourse from './component/deleteCourse';
const Mentor = () => {
  const navigate = useNavigate();
  const [data, setdata] = useState([]);
  const [total, setTotal] = useState(1);

  useEffect(() => {
    gettotalCourse(setTotal);
    getDataCourse(1, 10, setdata);
  }, []);
  // console.log(data);
  return (
    <>
      <Card>
        <div>
          <h2 style={{ fontSize: 40 }}>QUẢN LÝ KHÓA HỌC</h2>
          <Button
            style={{
              background: '#2c662c',
            }}
            shape="round"
            onClick={() => navigate(routerLinks('createCourse'))}
          >
            Tạo tài khoản
          </Button>
          <div>
            <Search />
          </div>

          <Table
            columns={columns(onDeleteCourse, setdata)}
            dataSource={data}
            pagination={false}
          />
          <Pagination
            total={total}
            showTotal={(total, range) => {
              return `${range[0]}-${range[1]} of ${total} items`;
            }}
            defaultPageSize={10}
            defaultCurrent={1}
            onChange={(page, pageSize) => {
              getDataCourse(page, pageSize, setdata);
            }}
          />
        </div>
      </Card>
    </>
  );
};
export default Mentor;
