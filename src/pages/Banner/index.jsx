import { AccountService } from '@/services/Account';
import { BannerService } from '@/services/banner';
import { Col, Row } from 'antd';
import _ from 'lodash';
import { useRef } from 'react';
import { useCallback, useEffect, useState } from 'react';
import UploadItem from './components/UploadItem';

const NUMBER_BANNERS = ['banner1', 'banner2', 'banner3', 'banner4'];

const BannerPage = () => {
  const [bannerImages, setBannerImages] = useState({});
  const [linkCategory, setLinkCategory] = useState();
  const [linkMentor, setLinkMentor] = useState();
  const [linkCourse, setLinkCourse] = useState();

  const isMounted = useRef(true);

  useEffect(() => {
    if (isMounted.current) {
      isMounted.current = false;
      try {
        const request = async () => {
          const response = await BannerService.getAllBanner();
          if (response?.success) {
            setBannerImages(_.keyBy(response?.banners, 'field_name'));
          }

          const responseCategoryLink =
            await BannerService.getBannerCategoryLink();
          if (responseCategoryLink?.success) {
            setLinkCategory(
              responseCategoryLink?.data?.map((link) => {
                return { key: link?.id, value: link?.name };
              })
            );
          }

          const responseMentor = await AccountService.getUser(4, 1, 1000);
          if (responseMentor?.success) {
            setLinkMentor(
              responseMentor?.users?.map((user) => {
                return { key: user?.id, value: user?.fullname };
              })
            );
          }

          const responseCourse =
            await BannerService.getBannerCategoryLinkCourse();
          if (responseCourse?.success) {
            setLinkCourse(
              responseCourse?.courses?.map((course) => {
                return { key: course?.id, value: course?.name_vi };
              })
            );
          }
        };
        request();
      } catch (error) {
        throw error;
      }
    }
  }, []);

  return (
    <Row justify="space-between">
      {NUMBER_BANNERS.map((id) => (
        <Col key={id} span={12} className="px-10 bg-white">
          <UploadItem
            keyBanner={id}
            initBanners={bannerImages}
            linkCategory={linkCategory}
            linkCourse={linkCourse}
            linkMentor={linkMentor}
          />
        </Col>
      ))}
    </Row>
  );
};

export default BannerPage;
