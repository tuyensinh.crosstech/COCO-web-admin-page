import { COURSE_API_PATH } from '@/constant/api';
import axiosClient from '../axiosClient';

export const CourseService = {
  getCourse: async (page, limit) => {
    try {
      // console.log(`${routerLinks(UserService.nameLink, 'api')}/login`);
      return axiosClient.get(
        `${COURSE_API_PATH}/getCoursesByPage?page=${page}&limit=${limit}`
      );
    } catch (e) {
      console.error(e);
      if (e.response.data.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  totalCourse: async () => {
    try {
      return axiosClient.get(`${COURSE_API_PATH}/getCountAllCourses`);
    } catch (e) {
      if (e.response.data.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  deleteCourse: async (id) => {
    try {
      return axiosClient.delete(`${COURSE_API_PATH}/deleteCourse/${id}`);
    } catch (e) {
      console.error(e);
      if (e.response.data.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
};
