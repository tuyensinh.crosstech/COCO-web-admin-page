import { BANNER_API_PATH } from '@/constant/api';
import axiosClient from './axiosClient';

export const BannerService = {
  getAllBanner: async () => {
    try {
      return axiosClient.get(`${BANNER_API_PATH}/getAllBanners`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  getBannerCategoryLink: async () => {
    return {
      data: [
        { id: 2, name: 'Mentor' },
        { id: 3, name: 'Course' },
      ],
      success: true,
    };
  },
  getBannerCategoryLinkCourse: async () => {
    try {
      return axiosClient.get(`courses/getCoursesByPage?page=1&limit=1000`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  getBannerCategoryLinkMentor: async () => {
    try {
      return axiosClient.get(`users/getAllUsers`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  uploadBanners: async (id, data, query) => {
    try {
      return axiosClient.post(
        `${BANNER_API_PATH}/uploadBanner?slot=${id}&${query}`,
        data,
        {
          headers: { 'Content-Type': 'multipart/form-data' },
        }
      );
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
  deleteBanner: async (id) => {
    try {
      return axiosClient.delete(`${BANNER_API_PATH}/deleteBanner/${id}`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.data.message });
      }
      return false;
    }
  },
};
