import axiosClient from '../axiosClient';
import routerLinks from '@/utils/router-links';
import { USER_API_PATH } from '@/constant/api';

export const AccountService = {
  getUser: async (role, page, limit) => {
    try {
      // console.log(`${routerLinks(UserService.nameLink, 'api')}/login`);
      return axiosClient.get(
        `${USER_API_PATH}/getUserByRole?role_id=${role}&page=${page}&limit=${limit}`
      );
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  lockAccount: async (id, action) => {
    console.log(id);
    try {
      // console.log(`${routerLinks(UserService.nameLink, 'api')}/login`);
      return axiosClient.put(
        `${USER_API_PATH}/lockUser?user_id=${id}&action=${action}`
      );
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  deleteAccount: async (id) => {
    try {
      return axiosClient.delete(`${USER_API_PATH}/deleteUser/${id}`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  userDetails: async (params) => {
    try {
      return axiosClient.get(`${USER_API_PATH}/getUserDetails/${params}`);
    } catch (e) {
      console.error(e);
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  totalPage: async (role, limit) => {
    try {
      return axiosClient.get(
        `${USER_API_PATH}/getUserByRolePageNumber?role_id=${role}&limit=${limit}`
      );
    } catch (e) {
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  totalUserRole: async (role) => {
    try {
      return axiosClient.get(`${USER_API_PATH}/getCountAllUserByRole/${role}`);
    } catch (e) {
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },

  mentorPending: async (page, limit) => {
    try {
      return axiosClient.get(
        `mentorRegistrations/getMentorRegistationsByPage?page=${page}&limit=${limit}`
      );
    } catch (e) {
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  confirmMentor: async (id, action) => {
    try {
      return axiosClient.put(
        `mentorRegistrations/updateMentorRegistration?id=${id}&action=${action}`
      );
    } catch (e) {
      if (e.response.message) {
        // Message.error({ text: e.response.message });
      }
      return false;
    }
  },
  getAllUser: async () => {
    try {
      return axiosClient.get(`${USER_API_PATH}/getAllUsers`);
    } catch (e) {
      console.error(e);
      // if (e.response.message) {
      //   // Message.error({ text: e.response.message });
      // }
      return false;
    }
  },
};
